import { drawCards } from "@/api/card.api";

class Deck {

    constructor(deckId, remaining) {
        this.deckId = deckId;
        this.remaining = remaining;
        return this;
    }

    drawCards(count = 1) {
        return drawCards(this.deckId, count);
    }

}

export default Deck;