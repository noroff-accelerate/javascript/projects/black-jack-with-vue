export const getCardValue = (card, flipAce = false) => {
    if (card.value === 'ACE') { // Not a number
        if (flipAce) {
            return  1;
        } else {
            return  11;
        }
    } else if (card.value.length > 2) { // Jack, Queen, King
        // 10
        return 10;
    } else {
        // It's already a number.
        return parseInt(card.value);
    }
}

export const isBust = score => score > 21;

export const hasAce = cards => {
    for (let i = 0; i < cards.length; i++) {
        if (cards[i].value === 'ACE') {
            return true;
        }
    }
    return false;
}

export const calculateHand = (cards, flipAce  = false) => {
    return cards.reduce((prev, next) => {
        const val1 = getCardValue(prev, flipAce);
        const val2 = getCardValue(next, flipAce);
        return {value: val1 + val2};
    }, {value: 0}).value
};