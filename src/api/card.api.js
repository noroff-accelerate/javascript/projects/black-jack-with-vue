const DECK_URL = 'https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count';

export const getDeck = (count = 1) => {
    return fetch(`${ DECK_URL }=${ count }`)
        .then(r => r.json())
        .then(r => {
            if (r.success === false) {
                throw Error('Could not fetch deck')
            }
            return r;
        });
};

export const drawCards = (deckId, count = 1) => {
    const url = `https://deckofcardsapi.com/api/deck/${deckId}/draw/?count=${count}`;
    return fetch(url)
        .then(r => r.json())
        .then(r => {
            if (r.success === false) {
                throw Error('Could not fetch deck')
            }
            return r;
        });
};

export const reshuffleDeck = deckId => {
    const url = `https://deckofcardsapi.com/api/deck/${ deckId }/shuffle/`;
    return fetch( url )
        .then(r => r.json())
        .then(r => {
            if (r.success === false) {
                throw Error('Could not fetch deck')
            }
            return r;
        });
}