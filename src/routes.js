import GameMenu from './components/GameMenu';
import GameRound from './components/GameRound';

import VueRouter from 'vue-router';
import Vue from 'vue';

Vue.use( VueRouter );

const routes = [
    {
        path: '/',
        name: 'GameMenu',
        component: GameMenu
    },
    {
        path: '/round',
        name: 'GameRound',
        component: GameRound
    },
];

export default new VueRouter({ routes })

